<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Sindhya Software</title>
	<link href="assets/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="assets/css/main.css" rel="stylesheet">
     

<link href='http://fonts.googleapis.com/css?family=Great+Vibes' rel='stylesheet' type='text/css'>
	 
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    
    <style>
  /* Note: Try to remove the following lines to see the effect of CSS positioning */
  .affix {
      top: 0;
      width: 100%;
  }

  .affix + .container-fluid {
      padding-top: 70px;
	   background-color:black;
  }
  </style>
    
    <script src="assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="assets/js/modernizr.custom.js"></script>
	<!-- core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon3.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body class="homepage">

    <?php
include "header.php"
?>  
<!--/header-->
<section id="about-slider"  class="no-margin">
                <div class="carousel slide">
				
                <div class="item active" style="background: #000 url(images/services/bg_services.jpg)">
                    <div class="container">
                        <div class="row slide-margin">
                         
                                <div class="carousels-content">
                                    <center><h1 class="animation animated-item-1">WE NEED YOUR FEEDBACK...</h1></center>
                          
                                 
                                </div>
                            

                          

                        </div>
                    </div>
                </div><!--/.item-->
				</div>
</section>	
    <section id="contact-info">
        <div class="center">                
            <h5>CONTACT US</h5>
        
        </div>
		
        <div class="gmap-area">
            <div class="container">
                <div class="row">
                    <div class="col-sm-5 text-center">
                        <div class="gmap">
                          <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15546.93209529865!2d80.2470816!3d13.0526583!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x4a4daa75057a4e32!2sSINDHYA+SOFTWARE+Pvt+Ltd!5e0!3m2!1sen!2sin!4v1499691578740" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
							</div>
</div>
                    <div class="col-sm-7 map-content">
                        <ul class="row">
                            <li class="col-sm-6">
                                <address>
                                    <h3>Head Office</h3>
								             
                                    <p>4th Floor,Shafika Building, <br>
								    kodambakkam High Road,<br>
									Nungambakkam,<br>
									Chennai-600034<br>
                                    Phone:+919788842101 <br>
                                    Email:info@sindhyasoftware.com</p>      <p> 
								
									
									
                                </address>

                               <!--  <address>
                                    <h3>Zonal Office</h3>
                                    <p>1/802 Muthamil Nagar <br>
                                    Natham Road,Dindigul-3                                
                                    Phone:+9788842111 <br>
                                    Email Address:sindhyasoftware@gmail.com</p>
                                </address> -->
                            </li>


                    </div>
                </div>
            </div>
        </div>
    </section>  <!--/gmap_area -->

    <section id="services" class="service-item">
        <div class="container">		<div class="center">        
                <p class="lead"><h1>MESSAGE US...</h1></p>
                <p class="lead">Thank you for showing interest in Sindhya software.
Fill this form and submit. 
Fields marked * are mandatory
</p></div>
            <div class="row contact-wrap"> 
                <div class="status alert alert-success" style="display: none"></div>
                <form id="main-contact-form" class="contact-form" name="contact-form" method="post" action="sendemail.php">
                    <div class="col-sm-5 col-sm-offset-1">
                        <div class="form-group">
                            <label><p class="lead">Name *</p></label>
                            <input type="text" name="name" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                            <label><p class="lead">Email *</p></label>
                            <input type="email" name="email" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                            <label><p class="lead">Phone</p></label>
                            <input type="number" class="form-control">
                        </div>
                        <div class="form-group">
                            <label><p class="lead">Company Name</p></label>
                            <input type="text" class="form-control">
                        </div>                        
                    </div>
                    <div class="col-sm-5">
                        <div class="form-group">
                            <label><p class="lead">Subject *</p></label>
                            <input type="text" name="subject" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                            <label><p class="lead">Message *</p></label>
                            <textarea name="message" id="message" required="required" class="form-control" rows="8"></textarea>
                        </div>                        
                        <div class="form-group">
                            <button type="submit" name="submit" class="btn btn-primary btn-lg" required="required">Submit Message</button>
                        </div>
                    </div>
                </form> 
				</div>
				</div>
            </div><!--/.row-->
        <!--/.container-->
		
		
		
		
		
		
		
    </section><!--/#contact-page-->

<?php include"footer.php" ?>
  <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/wow.min.js"></script>
	
		
	<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/retina.js"></script>
	<script type="text/javascript" src="assets/js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="assets/js/smoothscroll.js"></script>
	<script type="text/javascript" src="assets/js/jquery-func.js"></script>
</body>
</html>