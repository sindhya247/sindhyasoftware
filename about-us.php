<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Sindhya Software</title>
	<link href="assets/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="assets/css/main.css" rel="stylesheet">
     

<link href='http://fonts.googleapis.com/css?family=Great+Vibes' rel='stylesheet' type='text/css'>
	 
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    
    <style>
  /* Note: Try to remove the following lines to see the effect of CSS positioning */
  .affix {
      top: 0;
      width: 100%;
  }

  .affix + .container-fluid {
      padding-top: 70px;
	   background-color:black;
  }
  </style>
    
    <script src="assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="assets/js/modernizr.custom.js"></script>
	<!-- core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon3.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body class="homepage">

    <?php
include "header.php"
?>  
<!--/header-->
<section id="about-slider"  class="no-margin" >
                <div class="carousel slide">
				
                <div class="item active" style="background: #000 url(images/services/bg_services.jpg)">
                    <div class="container">
                        <div class="row slide-margin">
                         
                                <div class="carousels-content">
                                    <h1 class="animation animated-item-1">HAVING A STRONG PRESENCE ON THE WORLD WIDE WEB IS TODAY AN IMPERATIVE,</h1>
                                    <h1 class="animation animated-item-2">MORE THAN A REQUIREMENT....</h1>
                                 
                                </div>
                            

                          

                        </div>
                    </div>
                </div><!--/.item-->
				</div>
</section>	
			<section id="about-us">
        <div class="container">
			<div class="center wow fadeInDown">
				<h5>WHO WE ARE?</h5><br>

				<p class="lead1">Our company established with a view to enter the IT industry with the foresight in the year 1997.Since then , every day we have grown, evolved and metamorphosed to the present, gained in size and knowledge.
      We are justifiably proud of our reputation as a company who understand the needs of the customer and deliver high quality of service and product.
</p><br>
<p class="lead1">Our aim is to always to make products that are practical, cost effective and time efficient.We are globally recognized for its innovative approach towards delivering business value and its commitment to sustainability.
			</p><br>
			<p class="lead1">
			In today’s world, organizations will have to rapidly reengineer themselves and be more responsive to changing customer needs. Sindhya software is well positioned to be a partner and co-innovator to businesses in their transformation journey, identify new growth opportunities and facilitate their foray into new sectors and markets.
			</p>
			
			
	<div class="skill-wrap clearfix">
				
				<div class="row">
		
					<div class="col-sm-3">
						<div class="sinlge-skill wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
							<div class="joomla-skill">                                   
								<p><em>280+</em></p>
								<p>MUNICIPAL OFFICES,Town panchayats</p>
							</div>
						</div>
					</div>
					
					 <div class="col-sm-3">
						<div class="sinlge-skill wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
							<div class="html-skill">                                  
								<p><em>25+</em></p>
								<p>Schools are using our software</p>
							</div>
						</div>
					</div>
					
					<div class="col-sm-3">
						<div class="sinlge-skill wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="900ms">
							<div class="css-skill">                                    
								<p><em>3000+</em></p>
								<p>Schools using our site for mailing</p>
								
							</div>
						</div>
					</div>
					
					 <div class="col-sm-3">
						<div class="sinlge-skill wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="1200ms">
							<div class="wp-skill">
								<p><em>7+</em></p>
								<p>Distict CEO,DEO,IMS using our site </p>                                     
							</div>
						</div>
					</div>
					
				</div>
	
            </div><!--/.our-skill-->
				
				<p class="lead1">
			With this grand success and huge experience we stepped into the field of office automation for schools(website) couple of years ago .
			Sindhya software’s exclusively designed website(<a href="http://tnschoolsonline.in/" target="_blank" >www.tnschoolsonline.in</a>) ,
			which integrates each and every schools in the whole educational district marks our huge success in the field of educational software.
			Now as an achievement many districts such as <b>DINDIGUL,THIRUVALLUR,KANCHEPURAM etc</b> are currently using our website.
			
			
			</p>
			
				
				
				
				</div>
		
			</div><!--section-->
		</div><!--/.container-->
    </section><!--/about-us-->

<?php include"footer.php" ?>

    <script src="js/jquery.js"></script>
    <script type="text/javascript">
        $('.carousel').carousel()
    </script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/wow.min.js"></script>
		
	<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/retina.js"></script>
	<script type="text/javascript" src="assets/js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="assets/js/smoothscroll.js"></script>
	<script type="text/javascript" src="assets/js/jquery-func.js"></script>
</body>
</html>