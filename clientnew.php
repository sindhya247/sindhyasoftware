<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Sindhya Software</title>
	<link href="assets/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="assets/css/main.css" rel="stylesheet">
     <link rel="stylesheet" href="css/styles.css" type="text/css" />

<link href='http://fonts.googleapis.com/css?family=Great+Vibes' rel='stylesheet' type='text/css'>
	 
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    
    <style>
  /* Note: Try to remove the following lines to see the effect of CSS positioning */
  .affix {
      top: 0;
      width: 100%;
  }

  .affix + .container-fluid {
      padding-top: 70px;
	   background-color:black;
  }
  </style>
    
    <script src="assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="assets/js/modernizr.custom.js"></script>
	<!-- core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon3.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body class="homepage">


    <!-- Navigation -->
    
  <?php
include "header.php"
?>  


    <section id="about-slider"  class="no-margin" style="heigh:291px">
                <div class="carousel slide" style="    height: 480px;    margin-top: -113px;">
				
                <div class="item active" style="background: #000 url(ser/img/cc.png)">
                    <div class="container" style="    margin-top: -144px">
                        <div class="row slide-margin">
                         
                                <div class="carousels-content">
                                    <center><h1 class="animation animated-item-1"></h1></center>
                          
                                 
                                </div>
                            

                          

                        </div>
                    </div>
                </div><!--/.item-->
				</div>
</section>		
<section id="portfolio">
        <div class="container">
            <div class="center">
               <h2>Our Clients</h2>
               <!-- <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut <br> et dolore magna aliqua. Ut enim ad minim veniam</p>
           -->  </div>
        

            <ul class="portfolio-filter text-center">
                <li><a class="btn btn-default active" href="#" data-filter="*">All Clients</a></li>
				 <li><a class="btn btn-default" href="#" data-filter=".html">WEBSITE DEVELOPMENT</a></li>
                <li><a class="btn btn-default" href="#" data-filter=".bootstrap">SCHOOLS</a></li>
               
   
            </ul><!--/#portfolio-filter-->

            <div class="row">
                <div class="portfolio-items">
                    <div class="portfolio-item bootstrap col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="images/gallery1/recent/1.png" alt="">
                          
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item bootstrap col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="images/gallery1/recent/2.png" alt="">
                          
                        </div>          
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item bootstrap col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="images/gallery1/recent/3.png" alt="">
                           
                        </div>        
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item bootstrap col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="images/gallery1/recent/4.png" alt="">
                           
                        </div>           
                    </div><!--/.portfolio-item-->
					
					<div class="portfolio-item html col-xs-12 col-sm-4 col-md-3">
					<a href="http://njpcset.org/">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="images/gallery1/recent/w1.png" alt="">
							<span>www.njpcset.org</span>
                           
                        </div> 
</a>						
                    </div><!--/.portfolio-item-->
          
                    <div class="portfolio-item  bootstrap col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="images/gallery1/recent/5.png" alt="">
                          
                        </div>      
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item bootstrap col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="images/gallery1/recent/6.png" alt="">
                          
                        </div>         
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item bootstrap col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="images/gallery1/recent/7.png" alt="">
                         
                        </div>          
                    </div><!--/.portfolio-item-->
					<div class="portfolio-item html col-xs-12 col-sm-4 col-md-3">
					<a href="http://jescco.org/">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="images/gallery1/recent/w2.png" alt="">
							<span>www.jescco.org</span>
                           
                        </div> 
</a>						
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item bootstrap col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="images/gallery1/recent/8.png" alt="">
                        
                        </div>          
                    </div><!--/.portfolio-item-->
					 <div class="portfolio-item bootstrap col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="images/gallery1/recent/9.png" alt="">
                        
                        </div>          
                    </div><!--/.portfolio-item-->
					 <div class="portfolio-item bootstrap col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="images/gallery1/recent/10.png" alt="">
                        
                        </div>          
                    </div><!--/.portfolio-item-->
					 <div class="portfolio-item bootstrap col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="images/gallery1/recent/11.png" alt="">
                        
                        </div>          
                    </div><!--/.portfolio-item-->
					 <div class="portfolio-item bootstrap col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="images/gallery1/recent/12.png" alt="">
                        
                        </div>  
  </div><!--/.portfolio-item-->						
								<div class="portfolio-item html col-xs-12 col-sm-4 col-md-3">
					<a href="http://kalangarai.org/">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="images/gallery1/recent/w3.png" alt="">
							<span>www.kalangarai.org</span>
                           
                        </div> 
</a>						
                  
                    </div><!--/.portfolio-item-->
					 <div class="portfolio-item bootstrap col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="images/gallery1/recent/13.png" alt="">
                        
                        </div>          
                    </div><!--/.portfolio-item-->
					 <div class="portfolio-item bootstrap col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="images/gallery1/recent/14.png" alt="">
                        
                        </div>          
                    </div><!--/.portfolio-item-->
					<div class="portfolio-item html col-xs-12 col-sm-4 col-md-3">
					<a href="http://dietdindigul.in/">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="images/gallery1/recent/w8.png" alt="">
							<span>www.dietdindigul.in</span>
                           
                        </div> 
</a>						
                  
                    </div><!--/.portfolio-item--><div class="portfolio-item html col-xs-12 col-sm-4 col-md-3">
					<a href="http://tapasu.in/">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="images/gallery1/recent/w9.png" alt="">
							<span>www.tapasu.in</span>
                           
                        </div> 
</a>						
                  
                    </div><!--/.portfolio-item--><div class="portfolio-item html col-xs-12 col-sm-4 col-md-3">
					<a href="http://tnschoolsonline.in/">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="images/gallery1/recent/w11.png" alt="">
							<span>www.tnschoolsonline.in</span>
                           
                        </div> 
</a>						
                  
                    </div><!--/.portfolio-item--><div class="portfolio-item html col-xs-12 col-sm-4 col-md-3">
					<a href="http://holyspiritmatrimony.com/">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="images/gallery1/recent/w10.png" alt="">
							<span>www.holyspiritmatrimony.com</span>
                           
                        </div> 
</a>						
                  
                    </div><!--/.portfolio-item-->
					 <div class="portfolio-item bootstrap col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="images/gallery1/recent/15.jpg" alt="">
                        
                        </div>          
                    </div><!--/.portfolio-item-->
					<div class="portfolio-item html col-xs-12 col-sm-4 col-md-3">
					<a href="http://gttijohilpatti.in/">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="images/gallery1/recent/w4.png" alt="">
							<span>www.gttijohilpatti.in</span>
                           
                        </div> 
</a>						
                  
                    </div><!--/.portfolio-item-->
					<div class="portfolio-item bootstrap col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="images/gallery1/recent/16.png" alt="">
                        
                        </div>          
                    </div><!--/.portfolio-item-->
					<div class="portfolio-item bootstrap col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="images/gallery1/recent/17.png" alt="">
                        
                        </div>          
                    </div><!--/.portfolio-item-->
					<div class="portfolio-item bootstrap col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="images/gallery1/recent/18.jpg" alt="">
                        
                        </div>          
                    </div><!--/.portfolio-item-->
					<div class="portfolio-item html col-xs-12 col-sm-4 col-md-3">
					<a href="http://dietmadurai.in/index.html">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="images/gallery1/recent/w5.png" alt="">
							<span>www.dietmadurai.in</span>
                           
                        </div> 
</a>						
                  
                    </div><!--/.portfolio-item-->
					<div class="portfolio-item html col-xs-12 col-sm-4 col-md-3">
					<a href="http://dietvadalur.in/">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="images/gallery1/recent/w7.png" alt="">
							<span>www.dietvadalur.in</span>
                           
                        </div> 
</a>						
                  
                    </div><!--/.portfolio-item-->
					<div class="portfolio-item bootstrap col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="images/gallery1/recent/19.png" alt="">
                        
                        </div>          
                    </div><!--/.portfolio-item-->
					<div class="portfolio-item html col-xs-12 col-sm-4 col-md-3">
					<a href="http://dietpalayampatti.in/">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="images/gallery1/recent/w6.png" alt="">
							<span>www.dietpalayampatti.in</span>
                           
                        </div> 
</a>						
                  
                    </div><!--/.portfolio-item-->
					<div class="portfolio-item bootstrap col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="images/gallery1/recent/20.png" alt="">
                        
                        </div>          
                    </div><!--/.portfolio-item-->
					
                </div>
            </div>
        </div>
    </section><!--/#portfolio-item-->
	<!-- <p class="Tlead">  
	 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A course in Android is enough to make your resume speak for itself. Students, who are always on par with the recent trends in IT development, are the kind of people any company would be eager to recruit. Make yourself one such valuable professional by taking up a course in Android today.</p>
	
	<p class="Tlead">   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Mobility is considered as the next big thing in the IT Industry. Gartner Research Identifies "Mobile Centric Applications" as one of the Top 10 Strategic Technologies for 2012. </p><br>
            <p class="Tlead"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Android is an operating system made exclusively for use in Smart Phones and tablet computers. There are about 400 million Android phones in usage today occupying a market share of about 59%. Android Applications (Apps) extend the functionality of the devices and millions of apps are downloaded every day. This means Thousands of jobs exist to develop android and android based applications. These job opportunities are created not only in small and medium sized companies, but also in the leading MNC IT organizations.</p><br>
<p class="Tlead"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Introduction to programming for the Android Training course structure designed by Sindhya Software is to quickly get you up to speed with writing apps for Android devices. You will learn the basics of the Android platform with the Training provided by Sindhya Software and gain an understanding of the application lifecycle. By the end of the Android Training from Sindhya Software, you will be able to write simple GUI Android applications, use built-in widgets and components, work with the database to store data locally, and much more. This is a perfect course to get started with Android programming. This Android Training course is for students who wish to get up to speed with writing apps for Android devices.</p><br>

 <div class="width"STYLE="    MARGIN-LEFT: 40PX;">
        <FONT STYLE="font-weight:bold;font-color:red;WIDTH: 220PX;FONT-SIZE:20PX">ANDROID TRAINING</font><BR>
	Java Concepts<br>
	MySQL<br>
	Introduction TO Android<br>
	Application Structure(IN DETAIL)<br>
	Emulator-Android Virtual Device<br>
	Basic UI Design<br>
	Preferences<br>
	Menu<br>
	Intent(IN DETAIL)<br>
	UI Design<br>
	Tabs and TabActivity<br>
	Styles & Themes<br>
	Android Debug Bridge(ADB) Tool<br>
	Adapter And Widgtes<br>
    Notifications<br>
	Custom Components<br>
	Threads<br>
	       <font STYLE="font-weight:bold;font-color:red">DEVELOPING A NEW APP</font><br>

	</div>
 -->
				</section>

<?php
include "footer.php"
?>
	
	
	
    <!-- jQuery -->
<!--     <script src="ser/js/jquery.js"></script> -->

    <!-- Bootstrap Core JavaScript -->
   <!--  <script src="ser/js/bootstrap.min.js"></script> -->
	 <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/wow.min.js"></script>
	
		
	<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/retina.js"></script>
	<script type="text/javascript" src="assets/js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="assets/js/smoothscroll.js"></script>
	<script type="text/javascript" src="assets/js/jquery-func.js"></script>

</body>

</html>
