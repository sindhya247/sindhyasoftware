	
    <section id="bottom">
        <div class="container wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="widget">
                        <h3>Company</h3>
                        <ul>
                            <li><a href="about-us.php">About Us</a></li>
                            <li><a href="vision.php">Our Vision</a></li>
                            <li><a href="franchisee.php">Franchisee</a></li>
                           
                          <li><a href="contact-us.php">Contact Us</a></li>
                        </ul>
                    </div>    
                </div><!--/.col-md-3-->

               

                <div class="col-md-3 col-sm-6">
                    <div class="widget">
                        <h3>SERVICES</h3>
                        <ul>
                            <li><a href="s1.php">School Software</a></li>
                            <li><a href="s2.php">Mobile App</a></li>
                            <li><a href="s3.php">Bulk SMS</a></li>
                            <li><a href="s4.php">Bus Tracking</a></li>
							<li><a href="webdevelopment/index.php">Website Development</a></li>
							
                            <li><a href="http://tnschoolsonline.in/">Online Software</a></li>
                    
                        </ul>
                    </div>    
                </div><!--/.col-md-3-->
                
				<div class="col-md-3 col-sm-6">
                    <div class="widget">
                          <h3>Head Office</h3>
						<address>
                                  
                                          
                                    <p>4th Floor,Shafika Building, <br>
								    kodambakkam High Road,<br>
									Nungambakkam,<br>
									Chennai-600034<br>
                                    Phone:+919788842101 <br>
                                    Email:info@sindhyasoftware.com</p>
                                </address>
				     </div>    
                </div>		
 
           <!--      <div class="col-md-3 col-sm-6">
                    <div class="widget">
                          <h3>zonal Office</h3>
						    <address>
                                   
                                    <p>1/802, Muthamil Nagar <br>
                                    Natham Road,Dindigul-3    <br>                           
                                    Phone:+9788842111 <br>
                                    Email:sindhyasoftware@gmail.com</p>
                                </address>
				     </div>    
                </div>	
  -->
 
            </div>
        </div>
    </section><!--/#bottom-->   
   <footer id="footer" class="midnight-blue">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    &copy; 2017 <a target="_blank" href="http://www.sindhyasoftware.com" title="Sindhya Software">SINDHYA SOFTWARE</a>. All Rights Reserved.
                </div>
                <div class="col-sm-6">
                    <ul class="pull-right">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="about-us.php">About Us</a></li>
                       <li><a href="contact-us.php">Contact Us</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer><!--/#footer-->