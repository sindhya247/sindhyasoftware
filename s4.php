<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Sindhya Software</title>
	<link href="assets/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="assets/css/main.css" rel="stylesheet">
     

<link href='http://fonts.googleapis.com/css?family=Great+Vibes' rel='stylesheet' type='text/css'>
	 
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    
    <style>
  /* Note: Try to remove the following lines to see the effect of CSS positioning */
  .affix {
      top: 0;
      width: 100%;
  }

  .affix + .container-fluid {
      padding-top: 70px;
	   background-color:black;
  }
  </style>
    
    <script src="assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="assets/js/modernizr.custom.js"></script>
	<!-- core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon3.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body class="homepage">
<?php
include "header.php"
?>  
<!--/header-->
    <!-- Navigation -->
    
    <!-- Header -->
 	<section id="about-slider"  class="no-margin">
                <div class="carousel slide">
				
                <div class="item active" style="background: #000 url(ser/img/13.png)">
                    <div class="container">
                        <div class="row slide-margin">
                         
                                <div class="carousels-content">
                                    <center><h1 class="animation animated-item-1"></h1></center>
                          
                                 
                                </div>
                            

                          

                        </div>
                    </div>
                </div><!--/.item-->
				</div>
</section>	
	
    <!-- Page Content -->

	<a  name="services"></a>
    <div class="content-section-a" style="  margin-top: 1%">

        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-sm-6">
                    <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
                    <h2 class="section-heading">TRACKING SCHOOL BUS ..!</h2>
                    <p class="lead">
					        A properly integrated real-time GPS tracking system goes a long way in ensuring the safety of school children while 
							travelling in the school bus. It keeps you in the loop in real-time, notifying any unusual activity
The School Bus Tracker has a fully integrated GPS tracking system. Let’s see what the advantages the School Bus Tracker provides with its 
comprehensive tracking system.</p>
                    <p class="lead">   
					<ul><h2>BENEFIT THE EDUCATIONAL INSTITUTES BY</h2>
					<li>Real-time monitoring of transportation assets</li>
					<li>Improved conveyance management</li>
					<li>Regular monitoring to prevent delays in bus arrivals</li>
					<li>Tracking the halt, speed and idle state of buses</li>
					<li>Tracking students on board the bus</li>
					<li>Dealing with unforeseen situations</li></ul>
					

					
					</p>
					
                </div>
                <div class="col-lg-5 col-lg-offset-2 col-sm-6">
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.content-section-a -->

    <div class="content-section-b">

        <div class="container">

            <div class="row">
                <div class="col-lg-5 col-lg-offset-1 col-sm-push-6  col-sm-6">
                    <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
					<h2 class="section-heading">STUDENT SAFETY </h2>
                    <p class="lead">
                 
Get notified if a student steps into the wrong bus or if a student misses a stop and more. 
The primary aim of the whole software is undoubtedly the safety of students. 
You can enhance safety by monitoring speed, creating geo-fences and re-routing buses away from accidents.</p>
				<h2 class="section-heading">SAVINGS	</h2><p class="lead">
Control costs by reducing fuel usage, observing on-board engine diagnostics, automating driver logs/timesheets and more.</p>
            <h2 class="section-heading">STATE OF THE ART SOFTWARE & HARDWARE</h2><p class="lead">
The School Bus Tracker has a simple, user-friendly interface designed and developed in-house. 
We use high quality hardware from Taximeter.in, leading hardware provider of  Sindhya Software</p>

					
                </div>
				
                <div class="col-lg-5 col-sm-pull-6  col-sm-6">
                     <img class="img-responsive" src="ser/img/s41.png" alt="">
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.content-section-b -->

    <div class="content-section-a">

        <div class="container">

            <div class="row">
                <div class="col-lg-5 col-sm-6">
                    <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
					<h2 class="section-heading">REAL-TIME INFORMATION </h2>
                    <p class="lead">
						Undoubtedly the most obvious advantage, real-time information helps you know where your buses are, monitor 
						pick-up and drop-off. You will get information if one of the buses goes off route or if there are unscheduled stops, etc.
					</p>
	<h2 class="section-heading">COMMUNICATION </h2><p class="lead">
Two-way communication is a part of the software, and allows you to get in touch with the bus driver in case of any anomalies.</p>
<h2 class="section-heading">IMPROVED ACCURACY</h2><p class="lead">

The real-time information provided by the GPS system is saved. This info can be used to improve your map data, your routes and planning in future.

					</p>
					
					
                    
                </div>
                <div class="col-lg-5 col-lg-offset-2 col-sm-6">
              
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
 
    </div>


	<?php include"footer.php" ?>
	
	
    <!-- jQuery -->
<!--     <script src="ser/js/jquery.js"></script> -->

    <!-- Bootstrap Core JavaScript -->
   <!--  <script src="ser/js/bootstrap.min.js"></script> -->
	 <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/wow.min.js"></script>
	
		
	<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/retina.js"></script>
	<script type="text/javascript" src="assets/js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="assets/js/smoothscroll.js"></script>
	<script type="text/javascript" src="assets/js/jquery-func.js"></script>

</body>

</html>
