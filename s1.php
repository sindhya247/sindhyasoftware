<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Sindhya Software</title>
	<link href="assets/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="assets/css/main.css" rel="stylesheet">
     

<link href='http://fonts.googleapis.com/css?family=Great+Vibes' rel='stylesheet' type='text/css'>
	 
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    
    <style>
  /* Note: Try to remove the following lines to see the effect of CSS positioning */
  .affix {
      top: 0;
      width: 100%;
  }

  .affix + .container-fluid {
      padding-top: 70px;
	   background-color:black;
  }
  </style>
    
    <script src="assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="assets/js/modernizr.custom.js"></script>
	<!-- core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon3.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body class="homepage">

   <?php
include "header.php"
?>  
<!--/header-->
    <!-- Navigation -->
    
    <!-- Header -->
 <section id="about-slider"  class="no-margin">
                <div class="carousel slide">
				
                <div class="item active" style="background: #000 url(ser/img/s11.png)">
                    <div class="container">
                        <div class="row slide-margin">
                         
                                <div class="carousels-content">
                                    <center><h1 class="animation animated-item-1"></h1></center>
                          
                                 
                                </div>
                            

                          

                        </div>
                    </div>
                </div><!--/.item-->
				</div>
</section>
				
  
    <!-- Page Content -->

	<a  name="services"></a>
    <div class="content-section-a" style="  margin-top: 1%">

        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-sm-6">
                    <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
                    <h2 class="section-heading">ADMISSION MANAGEMENT </h2>
                    <p class="lead">The  admission  Management  software  module  manages  the  complete details  about  the  students  and  their  parents.
					It  efficiently executes  the  complete  admission  process  for  both  new  and  existing  old students  and  thus  those  students  are 
					automatically  treated  differently based  on  the  provided  information.Admission  cards  can  also  be generated.
					</p>
                </div>
                <div class="col-lg-5 col-lg-offset-2 col-sm-6">
                    <img class="img-responsive" src="ser/img/c2.png" alt="">
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.content-section-a -->

    <div class="content-section-b">

        <div class="container">

            <div class="row">
                <div class="col-lg-5 col-lg-offset-1 col-sm-push-6  col-sm-6">
                    <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
                    <h2 class="section-heading">ATTENDANCE MANAGEMENT<br></h2>
					<ul><li>STUDENT ATTENDANCE</li><li>STAFF ATTENDANCE</li></ul>
                    <p class="lead">
					Tedious task of student daily attendance can be done easily.Attendance data is auto calculated by the  software and can be shared easily with parents. 
					Auto alerts(sms to parents) for the absentees to encourage them to be regular to school.
<br>
	<ul><h2>ATTENDANCE REPORT	</h2>			
    <li>Overall Attendance Statement</li>
	<li>Day attendance</li>
	<li>Section wise Attendance Statement</li>
	<li>Student wise Attendance Statement</li>
	<li>Class wise attendance Statement</li>
	<li>Attendance Report </li>
	<li>Day attendance Report</li>
	<li>Class wise report</li>
	</ul>				



				</p>
                
				
				</div>
                <div class="col-lg-5 col-sm-pull-6  col-sm-6">
                    <img class="img-responsive" src="ser/img/c1.png" alt="">
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.content-section-b -->

    <div class="content-section-a">

        <div class="container">

            <div class="row">
                <div class="col-lg-5 col-sm-6">
                    <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
                    <h2 class="section-heading">FEES MANAGEMENT</h2>
                    <p class="lead">
					Fee management module automatically calculates the pending fees,payment details,deduction and other concessions offered by the school.
					we can also generate bill / fee receipt  which includes a  student copy  and school copy.
					And also it can be paid through any mode of payment such as cash/cheque/draft by student wise/class wise & section wise. 
					</p>
					<br>
					
                            <ul>
                               
                            
                       
					<h2>FEES REPORT</h2>
	<li>Fees Payment Reports</li>
	<li>Fees Circular (sms)</li>
	<li>Class Wise Fees Received Reports</li>
	<li>Section  Wise Fees Received Reports</li>
	<li>Other  Fees Received Reports</li>
	<li>Day Wise Fees Received Reports</li>
	<li>Class Wise Fees Received Reports</li>
	<li>Section Wise Fees Balance Report</li>
	<li>Extra  Fees Added Report</li>
	<li>Fees Added Report</li>
	<li>Fees Added Report (Single Entry)</li>
	<li>Report  For a Particular Fees</li>
	<li>Report  For a Particular Fees  (Class Wise)</li>
	<li>Fees Wise Report</li>
	<li>Fees Wise Report(Detaild)</li>
	<li>Fees Wise Report (Headwise)</li>
	<li>User Fees Wise Fees Report</li>

</ul>					
					
</li>					
					
                </div>
                <div class="col-lg-5 col-lg-offset-2 col-sm-6">
                    <img class="img-responsive" src="ser/img/cc3.png" alt="">
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.container a -->
<div class="content-section-b">

        <div class="container">

            <div class="row">
                <div class="col-lg-5 col-lg-offset-1 col-sm-push-6  col-sm-6">
                    <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
                    <h2 class="section-heading">Examination Management (CCE)</h2>
                    <p class="lead">
					It helps in planning, execution, maintenance and monitoring evaluation.The examination management system has the feature of generating customized reports
					and helps in making the process more transparent.Allotment of pass/fail marks can also be customized.
					It also maintains whole and complete profiles as how many marks obtained  term wise/class wise/subject wise/student wise.
					Automtically it enables the students total marks,min.
					marks for promotion.The report includes mark wise,grade wise,percentage wise,rank wise,subject wise grading.
					
					 <ul><li>Class wise Subject Master</li>
					 <li>Min-Max Marks Settings</li>
					 <li>Marks Calculation </li>
					 <li>Report Generation   </li>    
					<li>Scholastic Marks Entry</li>
					<li>Co-Scholastic Entry</li> 
					<li>Report Card (Term wise/Yearly)</li>
					<li>Consolidated Mark Sheet </li>
					</li>Auto Grade based on indicator selection</li>
					</ul>

					
					
					
					</p>
                </div>
                <div class="col-lg-5 col-sm-pull-6  col-sm-6">
                    <img class="img-responsive" src="ser/img/c7.png" alt="">
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
	 <!-- /.container b-->
    <div class="content-section-a">

        <div class="container">

            <div class="row">
                <div class="col-lg-5 col-sm-6">
                    <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
                    <h2 class="section-heading">PAYROLL MANAGEMENT </h2>
                    <p class="lead">
					The  School  Payroll Management Software  is  specially  designed for   remuneration/payment  for  employees/staff. 
					The  features of    School  Payroll Management Software includes    generation of  pay slips,    salary statements  , 
					professional  tax  statements  and  provident  fund statements.
					According to the users need it can be customized.The reports can also be generated.
					<ul><li>Paybill Setting</li>
	<li>Salary Settings</li>
	<li>Paybill Generation</li>
	<li>Fixed  Earnings & Deductions</li></ul>
	
	<ul><h2>PAYROLL REPORT</h2><li>Salary Settings Report</li>
	<li>Pay Slip Generate</li>
	<li>PF Payment Report</li>
	<li>PayBill Report ( Month Wise)</li></ul>


					</p>
                </div>
                <div class="col-lg-5 col-lg-offset-2 col-sm-6">
                    <img class="img-responsive" src="ser/img/c4.png" alt="">
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div><!-- /.container a-->
  <div class="content-section-b">

        <div class="container">

            <div class="row">
                <div class="col-lg-5 col-lg-offset-1 col-sm-push-6  col-sm-6">
                    <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
                    <h2 class="section-heading">TRANSPORTATION MANAGEMENT</h2>
                    <p class="lead">
					
					Transport Management features includes calculation of fuel consumption,taxes information,permit renewal date,route map of each vehicle,
					maintance expenditure report.
					It is also integrated into the fee structure and accounts so that the transport charges are automatically charged from the students.
					
					<ul><li>Vechicle Register</li>
<li>Transport Masters</li>
	<li>Vechicle Alloction</li>
	<li>Fees Settings</li>
	<li>Stutent  Alloction</li>
	<li>Driver  Alloction</li>
	<li>Service Register</li>
	<li>Log Book</li>
	<li>Stock Inword  Entry</li>
	<li>Stock Outword  Entry</li></ul>
	<ul><h2>TRANSPOTATION REPORT</h2>
	<li>Vechicle  Alloction Reports</li>
	<li>Stutent  Alloction  Reports</li>
	<li>Service  Reports  (SMS)</li>
	<li>Log   Book  Reports  </li>
	<li>Stock  Inword  Reports  </li>
	<li>Stock Outword  Reports  </li>
	<li>Final  Reports  </li>
	<li>Vehicles  Renewals  Reports  (SMS) </li>
	<li>Vehicles  Reports  </li>
	<li>Route  wise Stutent   Reports</li>  
	<li>Vehicles  wise Stutent   Reports  </li></ul>


					
					</p>
                </div>
                <div class="col-lg-5 col-sm-pull-6  col-sm-6">
                    <img class="img-responsive" src="ser/img/c6.png" alt="">
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
	
	
<div class="content-section-a">

        <div class="container">

            <div class="row">
                <div class="col-lg-5 col-sm-6">
                    <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
                    <h2 class="section-heading">STUDENT REPORTS</h2>
                    <p class="lead">
					
	<ul><li>Students   Reports</li>
	<li>Community Wise   Students   Reports</li>
	<li>Religion Wise  Students   Reports</li>
	<li>Income   Wise  Students   Reports</li>
	<li>Strength  Particulars</li>
	<li>Class Wise   Strength  Particulars</li></ul>
	<ul><h2>TC GENDRATON</h2>
	<li>TC  Apply</li>
	<li>TC Issue</li>
	</li>TC Issued  Report</li></ul>
	<ul><h2>OTHER CERTIFICATE</h2>
	<li>Conduct   Certificate</li>
	<li>Attendance   Certificate</li>
	<li>Bonafide   Certificate</li></ul>
</p>
				</div>
					<div class="col-lg-5 col-lg-offset-2 col-sm-6">
                    <img class="img-responsive" src="ser/img/c4.png" alt="">
					</div>
            </div>

		</div>
</div>	
  <div class="content-section-b">

        <div class="container">

            <div class="row">
                <div class="col-lg-5 col-lg-offset-1 col-sm-push-6  col-sm-6">
                    <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
                    <h2 class="section-heading">INVENTORY MANAGEMENT </h2>
                    <p class="lead">
The  Inventory  Management  Software  module  deals  with  stock  availability,  maintenance  of  stock  purchases,  sundry  purchases etc.
There  are  much more  features  like  generation  of  purchase  bill,  purchase  order,  delivery memo,  main  store  issue  and  return,stock  ledger,
fast  and  slow  moving items  etc.
Over the time, users can have annual reports  on  the  consumption  and  cost  factors  of  the  items  in  inventory.
	<ul><li>Item Master</li>
	<li>Supplier  Register</li>
	<li>Purchase Register</li>
	<li>Stock Issue</li></ul>
	
<ul><h2>INVENTORY REPORT</h2><li>Stock Overview</li>
<li>Stock  Report</li>
	<li>Purchase  Report</li>
	<li>Vendore Wise  Purchase  Report</li>
	<li>Stock  Issue  Report</li>
	<li>Department Wise   Stock  Issue  Report</li></ul>

	
	</p>
                
                </div>
                <div class="col-lg-5 col-sm-pull-6  col-sm-6">
                    <img class="img-responsive" src="ser/img/c6.png" alt="">
                </div>
            </div>

        </div>
		</div>
        <!-- /.container -->
		<div class="content-section-a">

        <div class="container">

            <div class="row">
                <div class="col-lg-5 col-sm-6">
                    <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
                    <h2 class="section-heading">ACCOUNT MANAGEMENT </h2>
                    <p class="lead">The  Accounts  Management software  consist  of  all  accounting  needs  of  the school.The features  includes  ranging from day books, 
					balance sheet accounts  ,schedule  balance,transportation  expense  employees  wages calculation,staff  salary  management,  trail  balance,
					income/expense statements,  general  ledger 
					and  also  allows  to  drill-downs  from balance  sheets  to  vouchers  and  automatically  manages  posting  of  fees into appropriate accounts heads .
					
	<ul>
	
<li>Account  Year  Creation</li>
	<li>Account  Group  Creation</li>
	<li>Bank  Account    Creation</li>
	<li>Cash Book Register</li>
	<li>Bank Book  Register</li></ul>

	
	<ul><h2>ACCOUNT REPORTS</h2>
	<li>Trail Balance</li>
	<li>Group  Summary</li>
	<li>Account  Summary</li>
	<li>Income  & Expenditure</li>
	<li>Bank Statement</li>
	<li>Cash Book   Statement</li>
	<li>Cash Book   Register  Report</li>
	<li>Bank Book  Register  Report</li></ul>

	
	
</p>
         </div>
                <div class="col-lg-5 col-lg-offset-2 col-sm-6">
                    <img class="img-responsive" src="ser/img/c4.png" alt="">
                </div>
            </div>

  </div>
	</div>	
		
		
		
		
<div class="content-section-b">

        <div class="container">

            <div class="row">
                <div class="col-lg-5 col-lg-offset-1 col-sm-push-6  col-sm-6">
                    <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
                    <h2 class="section-heading">CONTROL PANEL  </h2>
                    <p class="lead">The Control panel explains how to manage users and roles in System using the back-end of the Server. 
					As it is user friendly it will be simple and easy to handle.It includes illustrated instructions on all available operations
					using the Control Panel such as: Creating a User Account, Adding and Editing User Details, Changing the user details of the Current User,
					Deleting a User. Also, there are details on Creating and Deleting a Role.
					
	<ul>
	<li><b>Create Users:</b> You can create users  by the software.</li>
<li><b>User Groups:</b> It can be used among a group of employees in a single time.</li>
<li><b>User Rights:</b> Administration allows  an admin to have full control on delegation
of  authority  to  the various  employees  so  that  their  domain  of operation  may
be  restricted  to  their  own  department  only.  No  employee  is   able  to
see/edit/modify  the  information  that  is  not  permitted  to  his/her  role  in  the
organization. This  acts as  a great Management tool  and allows authorities to
have  direct  control  over  the  employees  access  to  the  software  and  to  the
school’s information resources in general</li>
<li><b>Change Password:</b>Multiple user  can  use their  separate  password  and also it lets them
 to change their password</li>

</p>
				 </div>
                <div class="col-lg-5 col-sm-pull-6  col-sm-6">
                    <img class="img-responsive" src="ser/img/c6.png" alt="">
                </div>
            </div>

        </div>
		</div>
	
	
	<?php include"footer.php" ?>
	
	
	  <script src="js/jquery.js"></script>
    <script type="text/javascript">
        $('.carousel').carousel()
    </script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/wow.min.js"></script>
	
		
	<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/retina.js"></script>
	<script type="text/javascript" src="assets/js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="assets/js/smoothscroll.js"></script>
	<script type="text/javascript" src="assets/js/jquery-func.js"></script>

<!-- 
 
    <script src="ser/js/bootstrap.min.js"></script>
	  -->

</body>

</html>
