<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Sindhya Software</title>
	<link href="assets/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="assets/css/main.css" rel="stylesheet">
     

<link href='http://fonts.googleapis.com/css?family=Great+Vibes' rel='stylesheet' type='text/css'>
	 
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    
    <style>
  /* Note: Try to remove the following lines to see the effect of CSS positioning */
  .affix {
      top: 0;
      width: 100%;
  }

  .affix + .container-fluid {
      padding-top: 70px;
	   background-color:black;
  }
  </style>
    
    <script src="assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="assets/js/modernizr.custom.js"></script>
	<!-- core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon3.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body class="homepage">
<?php
include "header.php"
?>  
<!--/header-->
    <!-- Navigation -->
    
    <!-- Header -->
 	<section id="about-slider"  class="no-margin">
                <div class="carousel slide">
				
                <div class="item active" style="background: #000 url(ser/img/bg6.jpg)">
                    <div class="container">
                        <div class="row slide-margin">
                         
                                <div class="carousels-content">
                                    <center><h1 class="animation animated-item-1">INDIA'S FIRST SCHOOL MANAGEMENT SOFTWARE INTEGRATED<br> WITH MOBILE APP TECHNOLOGY....</h1></center>
                          
                                 
                                </div>
                            

                          

                        </div>
                    </div>
                </div><!--/.item-->
				</div>
</section>	
	
    <!-- Page Content -->

	<a  name="services"></a>
    <div class="content-section-a" style="  margin-top: 1%">

        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-sm-6">
                    <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
                    <h2 class="section-heading">Yet An Another Milestone For SINDHYA SOFTWARE..!!!</h2>
                    <p class="lead">
					<b>SINDHYA SOFTWARE</b> is India's first school management software which comes with mobile apps for students, parents, teachers and management. 
					Sindhya software ensures smooth functioning of the day-to-day school operations.
					It enables parent-school interaction in an instant manner bridging all the possible communication gaps.
					To know more about our quality and features of our software please go through our demos at <a href=""> sms.sindhyasoftware.com  </a>
					</p>
                </div>
                <div class="col-lg-5 col-lg-offset-2 col-sm-6">
                    <img class="img-responsive" src="ser/img/p1.png" alt="">
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.content-section-a -->

    <div class="content-section-b">

        <div class="container">

            <div class="row">
                <div class="col-lg-5 col-lg-offset-1 col-sm-push-6  col-sm-6">
                    <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
                    <h2 class="section-heading">INDIVIDUAL TEACHER REMARKS</h2>
                    <p class="lead">   Student was active or down today? Whats the achievement? Has he done something good or bad? 
					Our android app enables the teacher to sent individual sms to specific 
					parent of a child regarding his marks,behavior etc.</p>
					
					
                </div>
				
                <div class="col-lg-5 col-sm-pull-6  col-sm-6">
                    <img class="img-responsive" src="ser/img/p2.png" alt="">
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.content-section-b -->

    <div class="content-section-a">

        <div class="container">

            <div class="row">
                <div class="col-lg-5 col-sm-6">
                    <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
					<h2 class="section-heading">DAILY ATTENDANCE </h2>
                    <p class="lead">
					The daily routine work of taking students attendance and sending sms to absentees parents is made simple.
					Once the attendance is taken by the class teacher through his/her android mobile using our SINDHYA SOFTWARE MOBILE 
					APP,sms will be instantly sent to the absentees parents within seconds automatically.
					Thus it saves time and the manpower needed for sending sms is reduced.
					</p>
					
					
                    
                </div>
                <div class="col-lg-5 col-lg-offset-2 col-sm-6">
                    <img class="img-responsive" src="ser/img/p3.png" alt="">
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
  
<div class="content-section-b">

        <div class="container">

            <div class="row">
                <div class="col-lg-5 col-lg-offset-1 col-sm-push-6  col-sm-6">
                    <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
               <h2 class="section-heading"> BULK  SMS</h2>
                    <p class="lead">
					The educational Institutes like Schools, Colleges, and Coaching Centers will benefit and save using    our    SINDHYA SOFTWARE  (sms.sindhyasoftware.com) 
					Web based SMS system. The benefit is not only derived for the Educational Institution, but also for the parents and for the Students. 
					The task for the School administration, management & the office Staff also becomes simple and streamlined. 
					We request you to read all the benefits which we have mentioned below. There still many benefits and there are multitudes of uses.
					
					<ul><h2>BENEFIT THE EDUCATIONAL INSTITUTES BY</h2>
	<li>Absenteeis Reports/Alert</li>

	<li>Attendance –Daily/Monthly</li>

	<li>Function Invitation</li>

	<li>Fee Dues</li>

	<li>Fee Schedules</li>

	<li>Exam Marks</li>

	<li>Exam Schedules</li>

	<li>Bus Schedule alerts –Cancellations, Late Arrivals, Bus Routes</li>

	<li>Advertisements –Admissions, School Festivals, New Facility/Program</li>

	<li>Emergency messages</li>

	<li>Parents - Teacher Meetings</li>

	<li>Performance Appraisal/Updates - Unit Test Results, Monthly Subject Ratings, Cumulative Rankings, etc.</li></ul>

					
					
					
					
					</p>
                </div>
                <div class="col-lg-5 col-sm-pull-6  col-sm-6">
                    <img class="img-responsive" src="ser/img/p4.png" alt="">
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <div class="content-section-a">

        <div class="container">

            <div class="row">
                <div class="col-lg-5 col-sm-6">
                    <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
					     <h2 class="section-heading">CIRCULARS & NOTICES</h2>
                    <p class="lead"> Class teachers can send circulars and notices regarding various 
					school functions,parents meeting and fee due particulars using our mobile app instantly</p>
					
                    
                </div>
                <div class="col-lg-5 col-lg-offset-2 col-sm-6">
                    <img class="img-responsive" src="ser/img/p5.png" alt="">
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
	<!--
  <div class="content-section-b">

        <div class="container">

            <div class="row">
                <div class="col-lg-5 col-lg-offset-1 col-sm-push-6  col-sm-6">
                    <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
                    <h2 class="section-heading">3D Device Mockups<br>by PSDCovers</h2>
                    <p class="lead">Turn your 2D designs into high quality, 3D product shots in seconds using free Photoshop actions by <a target="_blank" href="http://www.psdcovers.com/">PSDCovers</a>! Visit their website to download some of their awesome, free photoshop actions!</p>
                </div>
                <div class="col-lg-5 col-sm-pull-6  col-sm-6">
                    <img class="img-responsive" src="ser/img/p6.png" alt="">
                </div>
            </div>

        </div> -->
        <!-- /.container --> 

  <!--  </div> -->
<?php include"footer.php" ?>
	
	
    <!-- jQuery -->
    <!-- <script src="ser/js/jquery.js"></script> -->
 <script type="text/javascript">
        $('.carousel').carousel()
    </script>
    <!-- Bootstrap Core JavaScript -->

	 <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/wow.min.js"></script>
	
		
	<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/retina.js"></script>
	<script type="text/javascript" src="assets/js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="assets/js/smoothscroll.js"></script>
	<script type="text/javascript" src="assets/js/jquery-func.js"></script>

</body>

</html>
