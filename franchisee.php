<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Sindhya Software</title>
	<link href="assets/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="assets/css/main.css" rel="stylesheet">
     

<link href='http://fonts.googleapis.com/css?family=Great+Vibes' rel='stylesheet' type='text/css'>
	 
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    
    <style>
  /* Note: Try to remove the following lines to see the effect of CSS positioning */
  .affix {
      top: 0;
      width: 100%;
  }

  .affix + .container-fluid {
      padding-top: 70px;
	   background-color:black;
  }
  </style>
    
    <script src="assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="assets/js/modernizr.custom.js"></script>
	<!-- core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon3.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body class="homepage">

    <?php
include "header.php"
?>  
<!--/header-->
<section id="about-slider"  class="no-margin">
                <div class="carousel slide">
				
                <div class="item active" style="background: #000 url(images/services/bg_services.jpg)">
                    <div class="container">
                        <div class="row slide-margin">
                         
                                <div class="carousels-content">
                                    <center><h1 class="animation animated-item-1">CONNECT WITH SUCCESS...</h1></center>
                          
                                 
                                </div>
                            

                          

                        </div>
                    </div>
                </div><!--/.item-->
				</div>
</section>	
			<section id="about-us">
        <div class="container">
			<div class="center wow fadeInDown">
				<h5>FRANCHISEE</h5><br>
<p class="lead"> <br>&nbsp;Sindhya software is keen to scale its presence for which we invite commercial business partners <br>who share our passion to impart quality IT education and training.

</p>
			
			</div>
	
		<h4>Basic requirements for becoming a franchisee:</h4>
•	A minimum of 800 to 1200 sq.ft area in a prime commercial location<br>
•	Financial capability to invest in basic infrastructure [like systems, software and furniture<br>
•	Flair for business and resources management<br>
•	Commitment to deliver quality education and training<br>
•	Preferably prior experience / background in Information Technology<br>
<h4>Value Propositions offered by Sindhya software  to Franchisees</h4>
Franchisees get the advantage of leveraging the ‘Sindhya Software’ brand name to develop and scale their business. Apart from this, we provide comprehensive support on the below-given aspects to ensure that our association is mutually beneficial.</br>
•	<b>Start-up</b> support in terms of commencement of business, setting up of infrastructure, manpower recruitment and training will be given. <br>
•	<b>Marketing</b> support in terms of participation in events, local promotional activities and collaterals such as sales flyers, prospectus, course information brochures and other advertising material at a nominal cost.<br>
•	<b>Academic support</b> in the form of well-compiled course modules, faculty training and grooming, conduct of unit tests [including question banks and answer keys], project walk-throughs and periodic technological upgradation at a nominal fee.<br>
•	<b>Operational support</b> in the form of admission procedures, branch operation manuals, batch scheduling, calendar planning, conducting examinations [both regular and for certification purposes], evaluation methodology, students/course records maintenance and placement planning.<br>
</div>
</section>
<section id="services" class="service-item">
        <div class="container">
			<div class="center">        
                <p class="lead"><h1>FRANCHISEE ENQUIRY FORM</h1></p>
                <p class="lead">Thank you for showing interest in Sindhya software.
Fill this form and submit. 
Fields marked * are mandatory
</p>
            </div> 
            <div class="row contact-wrap"> 
                <div class="status alert alert-success" style="display: none"></div>
                <form id="main-contact-form" class="contact-form" name="contact-form" method="post" action="sendemail.php">
                    <div class="col-sm-5 col-sm-offset-1">
                        
						
                        
						<div class="form-group">
                            <label><p class="lead">Fathers Name  *</p></label>
                            <input type="text" name="name" class="form-control" required="required">
                        </div>
						
                        <div class="form-group">
                            <label><p class="lead">Date Of Birth</p></label>
                            <input type="text" class="form-control">
                        </div>
						<div class="form-group">
                            <label><p class="lead">Address for Communication*</p></label>
                            <textarea name="message" id="message" required="required" class="form-control" rows="8"></textarea>
                        </div>  
						<div class="form-group">
                            <label><p class="lead">Contact Number</p></label>
                            <input type="number" class="form-control">
                        </div>
						<div class="form-group">
                            <label><p class="lead">Email *</p></label>
                            <input type="email" name="email" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                            <label><p class="lead">Educational Qualification*</p></label>
                            <input type="text" class="form-control">
                        </div>                        
                    <div class="form-group">
                            <label><p class="lead">Location Interested in*</p></label>
                            <input type="text" class="form-control">
                        </div>                        
                    </div>
					
					
                    <div class="col-sm-5">
                        <div class="form-group">
                            <label><p class="lead">How do you know Sindhya software*</p></label>
                            <input type="text" name="subject" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                            <label><p class="lead">Why are you interested in partnering with sindhya software( Max 250 words )*</p></label>
                            <textarea name="message" id="message" required="required" class="form-control" rows="8"></textarea>
                        </div>     

						
                        <div class="form-group">
                            <button type="submit" name="submit" class="btn btn-primary btn-lg" required="required">Send</button>
                        </div>
                    </div>
                </form> 
            </div><!--/.row-->
		</div><!--/.container-->
    </section><!--/about-us-->
	
<?php include"footer.php" ?>
    

    <script src="js/jquery.js"></script>
    <script type="text/javascript">
        $('.carousel').carousel()
    </script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/wow.min.js"></script>
	
		
	<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/retina.js"></script>
	<script type="text/javascript" src="assets/js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="assets/js/smoothscroll.js"></script>
	<script type="text/javascript" src="assets/js/jquery-func.js"></script>
</body>
</html>